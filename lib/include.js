/**
 * Created by zhoulin on 2019/3/7.
 */

//判断为空
function empty(str) {
    str = $.trim(str);
    return (str == "" || str == 'undefined' || str == "null" || str == null || typeof(str) == undefined);
}

storageKey = {
    sessionStorageKey:{
        token:"_TOKEN"
    },
    localStorageKey:{

    },
}

//h5 本地存储
sessionStorageUtils = {
    //设置存储
    setParam : function (name,value){
        sessionStorage.setItem(name,value)
    },
    //设置存储json字符串
    setJsonStr : function (name,value) {
        sessionStorageUtils.setParam(name,"'"+JSON.stringify(value));
    },
    //获取存储
    getParam : function(name){
        return sessionStorage.getItem(name)
    },
    //获取json对象存储
    getJson:function (name) {
        console.log(name)
        var param = sessionStorageUtils.getParam(name);
        return empty(param) ? null : JSON.parse(param.substr(1,param.length));
    },
    /**
     * 删除存储
     */
    del:function(name){
        sessionStorage.removeItem(name);
    },
}

var _TOKEN = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1NTMyMjg2NTYsInVzZXJKc29uU3RyIjoie1wiYXBwSWRcIjoxLFwiaWRcIjoxLFwibmlja25hbWVcIjpcIuiAgeeOi1wiLFwic3RhdHVzXCI6MSxcInR5cGVcIjowLFwidXNlcm5hbWVcIjpcImFkbWluXCJ9In0.y-973rUO7sTRQHeqvXwN49seSl2QfQbbY-8H42esF2g";

//sessionStorageUtils.setJsonStr(storageKey.sessionStorageKey.token,_TOKEN);

 var publicCssJs = '<meta charset="utf-8">'
            +'<meta name="viewport" content="width=device-width, initial-scale=1.0">'
    +'<link href="css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">'
    +'<link href="css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">'
    +'<link href="css/plugins/bootstrap-table/bootstrap-table.min.css" rel="stylesheet">'
    +'<link href="css/animate.min.css" rel="stylesheet">'
    +'<link href="css/style.min862f.css?v=4.1.0" rel="stylesheet">'
    +'<style>[v-cloak] { display: none;}</style>'
    +'<script src="js/jquery.min.js?v=2.1.4"></script>'
    +'<script src="js/bootstrap.min.js?v=3.3.6"></script>'
    +'<script src="js/content.min.js?v=1.0.0"></script>'
    +'<script src="js/plugins/bootstrap-table/bootstrap-table.min.js"></script>'
    +'<script src="js/plugins/bootstrap-table/bootstrap-table-mobile.min.js"></script>'
    +'<script src="js/plugins/bootstrap-table/locale/bootstrap-table-zh-CN.min.js"></script>'
    +'<script src="js/vue/vue.js"></script>'
    +'<script src="js/vue/vue-resource.js"></script>'
    +'<script src="js/demo/bootstrap-table-demo.min.js"></script>'
    +'<script>Vue.http.headers.common["Authorization"] = sessionStorageUtils.getJson(storageKey.sessionStorageKey.token) </script>'

document.write(publicCssJs);